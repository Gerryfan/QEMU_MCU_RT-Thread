# 简介

介绍 stm32f411 NUCLEO 使用 RT-Thread 在linux 下基于 QEMU 环境的应用。



# 1.环境搭建

[参考STM32F429](https://github.com/cbhust/STM32F429_Discovery_FreeRTOS_9)

[**参考在 Ubuntu 平台开发 RT-Thread**](https://www.rt-thread.org/document/site/application-note/setup/qemu/ubuntu/an0005-qemu-ubuntu/)

*先将install 下文件复制到ubuntu 环境下的Downloads 文件夹下。*

1. apt-get install libfdt-dev
2. 设置qemu-system-gnuarmeclipse 路径

root@ubuntu:/home/cpk/work/stm32f411# 

export PATH=/home/cpk/Downloads/install/usr/local/bin/:$PATH

3. qemu-system-gnuarmeclipse --version

4. scons 编译。

   ![scons](./stm32f411/figures/scons.PNG)

5. qemu-system-gnuarmeclipse -s --board NUCLEO-F411RE --mcu STM32F411RE --image rtthread.elf 

   ![cmd](./stm32f411/figures/cmd.PNG)

6. GPIO 及UART实际效果：

   ![效果](./stm32f411/figures/gpio.PNG)

   

   

   # 2. GDB调试

   1. 设置arm-none-eabi-gdb 路径

      export PATH=/opt/gcc-arm-none-eabi-6_2-2016q4/bin/:$PATH

   2. 运行如下命令：

   ```
   qemu-system-gnuarmeclipse --verbose --verbose --board NUCLEO-F411RE --mcu STM32F411RE --gdb tcp::1234 -S -d unimp,guest_errors  --image rtthread.elf --semihosting-config enable=on,target=native --semihosting-cmdline hello_rtos 1 2 3
   
   ```

   

   3. qemu启动后等待来自gdb的调试指令，打开另外一个终端窗口，运行

   ```
   #arm-none-eabi-gdb
   ```
   在gdb界面内运行：
   ```
   (gdb)target remote localhost:1234
   (gdb)continue
   ```
   可以看到qemu开始执行程序，GUI界面上可以看到程序执行效果。

   break 可以直接想要观察的函数位置设置断点。

   

# 用途：

1. 研究内核源码，还能仿真；
2. 目前代码支持uart 及GPIO 中断。

# 问题：

1.不支持浮点运算：

qemu-system-gnuarmeclipse: Attempt to set CP10/11 in SCB->CPACR, but FP is not supported yet.

修改rtconfig.py 中：

```
    DEVICE = ' -mcpu=cortex-m4 -mthumb -mfpu=fpv4-sp-d16 -mfloat-abi=soft -ffunction-sections -fdata-sections'
```











