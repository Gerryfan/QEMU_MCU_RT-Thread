/*
 * Copyright (c) 2006-2018, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-06     SummerGift   first version
 */

#include <rtthread.h>
#include <rtdevice.h>
#include <board.h>

/* defined the LED0 pin: PA5 */
#define LED0_PIN               GET_PIN(A, 5)
#define USER_KEY_PIN           GET_PIN(C,13)
void irq_callback(void *args)
{
        rt_kprintf("user key is pressed \n");
        if(rt_pin_read(LED0_PIN) == PIN_LOW)
          rt_pin_write(LED0_PIN,PIN_HIGH);
        else
        {
            rt_pin_write(LED0_PIN,PIN_LOW);
        }
        
}
int main(void)
{
    int count = 1;
    /* set LED0 pin mode to output */
    rt_pin_mode(LED0_PIN, PIN_MODE_OUTPUT);
    rt_pin_mode(USER_KEY_PIN, PIN_MODE_INPUT_PULLUP);
    rt_pin_attach_irq(USER_KEY_PIN,PIN_IRQ_MODE_FALLING,irq_callback,(void *)USER_KEY_PIN);
    rt_pin_irq_enable(USER_KEY_PIN,PIN_IRQ_ENABLE);

    while (count++)
    {
//        rt_pin_write(LED0_PIN, PIN_HIGH);
        // if(rt_pin_read(USER_KEY_PIN) == PIN_LOW)
        // {
        //     rt_thread_mdelay(50);
        //     if( rt_pin_read(USER_KEY_PIN) == PIN_LOW )
        //     {
        //         rt_kprintf("user key is pressed \n");
        //         rt_pin_write(LED0_PIN,PIN_HIGH);
        //     }
        // }
        // else
        // {
        //     rt_pin_write(LED0_PIN,PIN_LOW);
        // }
        rt_thread_mdelay(10);
//      rt_pin_write(LED0_PIN, PIN_LOW);
      //  rt_thread_mdelay(500);
    }

    return RT_EOK;
}
