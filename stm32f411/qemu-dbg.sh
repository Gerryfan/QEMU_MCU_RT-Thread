#!/bin/bash


#qemu-system-arm -M vexpress-a9 -kernel rtthread.elf -serial vc -serial vc -sd s#d.bin -S -s

qemu-system-gnuarmeclipse --verbose --verbose --board NUCLEO-F411RE --mcu STM32F411RE --gdb tcp::1234 -S -d unimp,guest_errors  --image rtthread.elf --semihosting-config enable=on,target=native --semihosting-cmdline hello_rtos 1 2 3

#qemu-system-gnuarmeclipse --verbose --verbose --board STM32F429I-Discovery --#mcu STM32F429ZI --gdb tcp::1234 -S -d unimp,guest_errors  --image #hello_rtos.elf --semihosting-config enable=on,target=native --semihosting-#cmdline hello_rtos 1 2 3
